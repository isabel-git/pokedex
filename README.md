# Pokedex

A Flutter-based Pokedex application showcasing various Pokemon with detailed information and images.

## Features

- Browse through a list of Pokemon.
- View detailed information about each Pokemon.
- Responsive design for optimal viewing on different devices.

## Screenshots


### Home Screen and Drawer
<div align="center">
  <img src="screenshots/home.png" alt="Home Screen" width="200" />
  <img src="screenshots/drawer.png" alt="Drawer" width="200" />
</div>

### Pokemon and Details Screen
<div align="center">
  <img src="screenshots/pokemon-generation.png" alt="Pokemon Generation" width="200" />
  <img src="screenshots/pokemon-details.png" alt="Pokemon Details" width="200" />
</div>

### Favorites Pokemon and Settings Screen
<div align="center">
  <img src="screenshots/favorites.png" alt="Pokemon Details" width="200" />
  <img src="screenshots/settings.png" alt="Pokemon Details" width="200" />
</div>

## Installation
1. **Add Flutter to your machine:**
Open the [documentation](https://docs.flutter.dev/) and install flutter

2. **Clone the repository:**
`git clone https://gitlab.com/isabel-git/pokedex.git`

3. **Navigate to the project directory:**
`cd pokedex`

4. **Install dependencies:**
`flutter pub get`

5. **Run the app:**
`flutter run`

## Usage

Open the app and start exploring various Pokemon.
