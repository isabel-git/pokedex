import 'package:flutter/material.dart';
import 'package:pokedex/models/pokemon.dart';
import 'package:pokedex/screens/pokemon_details.dart';
import 'package:pokedex/widget/pokemon_item.dart';

class Pokemons extends StatelessWidget {
  const Pokemons({
    super.key,
    this.title,
    required this.pokemon,
  });

  final String? title;
  final List<Pokemon> pokemon;

  selectPokemon(Pokemon p, BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (ctx) => PokemonDetails(
          pokemon: p,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    Widget content = Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            'Oh no... Non c\'è niente!',
            style: theme.textTheme.headlineLarge!
                .copyWith(color: theme.colorScheme.onSurface),
          ),
          Text(
            'Prova a selezionare qualcos\'altro',
            style: theme.textTheme.bodyLarge!
                .copyWith(color: theme.colorScheme.onSurface),
          ),
        ],
      ),
    );

    if (pokemon.isNotEmpty) {
      content = GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
        ),
        itemCount: pokemon.length,
        itemBuilder: (ctx, index) => PokemonItem(
          pokemon: pokemon[index],
          onSelectPokemon: (pokemon) {
            selectPokemon(pokemon, context);
          },
        ),
      );
    }

    if (title == null) {
      return content;
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(title!),
      ),
      body: content,
    );
  }
}
