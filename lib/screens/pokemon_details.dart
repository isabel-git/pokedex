import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pokedex/models/pokemon.dart';
import 'package:pokedex/providers/favorites_provider.dart';
import 'package:pokedex/widget/pokemon_detail.dart';
import 'package:pokedex/widget/pokemon_item_trait.dart';

class PokemonDetails extends ConsumerWidget {
  const PokemonDetails({super.key, required this.pokemon});

  final Pokemon pokemon;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final ThemeData theme = Theme.of(context);
    final favoritesPokemons = ref.watch(favoritesProvider);
    final isFavorite = favoritesPokemons.contains(pokemon);
    return Scaffold(
      appBar: AppBar(
        title: Text(pokemon.name),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 32),
            child: IconButton(
              onPressed: () {
                //all'interno di funzioni come onPressed si utilizza read per leggere i valori del provider
                final wasAdded = ref
                    .read(favoritesProvider.notifier)
                    .toggleFavoritePokemon(pokemon);
                ScaffoldMessenger.of(context).clearSnackBars();
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    duration: const Duration(seconds: 1),
                    content: Text(wasAdded
                        ? 'Pokemon aggiunto ai preferiti'
                        : 'Rimosso dai preferiti'),
                  ),
                );
              },
              // AnimatedSwitcher ci consente di animare la transazione da un widget all'altro
              // l'animazione verra' eseguita ogni volta che il widget si aggiorna o cambia
              icon: AnimatedSwitcher(
                duration: const Duration(
                  microseconds: 300,
                ),
                // transitionBuilder indica come vogliamo animare il widget
                transitionBuilder: (child, animation) {
                  return RotationTransition(
                    // turns indica il valore dei giri che dovra' fare child
                    turns: Tween(begin: 0.5, end: 1.0).animate(animation),
                    child: child,
                  );
                },
                child: Icon(
                  isFavorite ? Icons.star : Icons.star_border,
                  // key permette a flutter di distinguere i widget
                  key: ValueKey(isFavorite),
                ),
              ),
            ),
          )
        ],
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Hero(
                  tag: pokemon.id,
                  child: Image.network(
                    pokemon.imageUrl,
                    width: double.infinity,
                    fit: BoxFit.cover,
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      pokemon.description,
                      style: theme.textTheme.titleLarge!.copyWith(
                        color: theme.colorScheme.onSurface,
                      ),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              PokemonDetail(title: 'ID', lable: pokemon.id),
                              const SizedBox(height: 14),
                              PokemonDetail(
                                  title: 'CATEGORY', lable: pokemon.category),
                            ],
                          ),
                        ),
                        PokemonItemTrait(types: pokemon.type),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
