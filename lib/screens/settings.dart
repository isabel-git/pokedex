import 'package:flutter/material.dart';
import 'package:pokedex/providers/filters_provider.dart';
import 'package:pokedex/widget/setting_item.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class SettingsScreen extends ConsumerWidget {
  const SettingsScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final activeFilters = ref.watch(filtersProvider);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Impostazioni'),
      ),
      body: Column(children: [
        SettingItem(
          title: 'Forma Base',
          subtitle: 'Mostra solo i pokemon al primo stadio evolutivo',
          value: activeFilters[Filter.baseStats]!,
          handleChange: (isCkecked) {
            ref
                .read(filtersProvider.notifier)
                .setFilter(Filter.baseStats, isCkecked);
          },
        ),
        SettingItem(
          title: 'Leggendari',
          subtitle: 'Mostra solo i pokemon leggendari',
          value: activeFilters[Filter.legendary]!,
          handleChange: (isCkecked) {
            ref
                .read(filtersProvider.notifier)
                .setFilter(Filter.legendary, isCkecked);
          },
        ),
        SettingItem(
          title: 'Mitici',
          subtitle: 'Mostra solo i pokemon mitici',
          value: activeFilters[Filter.mytical]!,
          handleChange: (isCkecked) {
            ref
                .read(filtersProvider.notifier)
                .setFilter(Filter.mytical, isCkecked);
          },
        ),
        SettingItem(
          title: 'Varianti regionali',
          subtitle: 'Mostra solo i pokemon di categoria varianti regionali',
          value: activeFilters[Filter.regionalVariant]!,
          handleChange: (isCkecked) {
            ref
                .read(filtersProvider.notifier)
                .setFilter(Filter.regionalVariant, isCkecked);
          },
        ),
      ]),
    );
  }
}
