import 'package:flutter/material.dart';
import 'package:pokedex/providers/favorites_provider.dart';
import 'package:pokedex/providers/filters_provider.dart';
import 'package:pokedex/screens/generations.dart';
import 'package:pokedex/screens/pokemons.dart';
import 'package:pokedex/screens/settings.dart';
import 'package:pokedex/widget/main_drawer.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

const Map<Filter, bool> kInitialFilteres = {
  Filter.baseStats: false,
  Filter.legendary: false,
  Filter.mytical: false,
  Filter.regionalVariant: false,
};

class TabsScreen extends ConsumerStatefulWidget {
  const TabsScreen({super.key});

  @override
  ConsumerState<TabsScreen> createState() => _TabsScreenState();
}

class _TabsScreenState extends ConsumerState<TabsScreen> {
  int _selectedPageIndex = 0;

  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  void _setScreen(String identifier) async {
    //if you click on the pokedex option then you will come back
    Navigator.of(context).pop();
    // if you click on the settings option then you will enter to Settings Screen
    if (identifier == 'Impostazioni') {
      //when you use <> you can indicate which value will be returned by push
      await Navigator.of(context).push<Map<Filter, bool>>(
        // when you use pushReplacement insted push, the user cannot return to the previous screen using the Back button
        MaterialPageRoute(
          builder: (context) => const SettingsScreen(),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final availablePokemon = ref.watch(filteredPokemonsProvider);

    Widget activePage = GenerationsScreen(
      availablePokemon: availablePokemon,
    );

    String activeTitle = 'Generations';

    if (_selectedPageIndex == 1) {
      //watch osserva di continuo i cambiamentii del provider
      final favoritePokemons = ref.watch(favoritesProvider);
      activePage = Pokemons(
        pokemon: favoritePokemons,
      );
      activeTitle = 'Favorites';
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(activeTitle),
      ),
      drawer: MainDrawer(onSelectScreen: _setScreen),
      body: activePage,
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectPage,
        currentIndex: _selectedPageIndex,
        items: const [
          BottomNavigationBarItem(
              icon: Icon(Icons.catching_pokemon), label: 'Generations'),
          BottomNavigationBarItem(icon: Icon(Icons.star), label: 'Favorites'),
        ],
      ),
    );
  }
}
