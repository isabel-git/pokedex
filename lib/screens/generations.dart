import 'package:flutter/material.dart';
import 'package:pokedex/data/generations_data.dart';
import 'package:pokedex/models/generation.dart';
import 'package:pokedex/models/pokemon.dart';

import 'package:pokedex/screens/pokemons.dart';
import 'package:pokedex/widget/generation_item.dart';

class GenerationsScreen extends StatefulWidget {
  const GenerationsScreen({super.key, required this.availablePokemon});
  final List<Pokemon> availablePokemon;

  @override
  State<GenerationsScreen> createState() => _GenerationsScreenState();
}

//with e' una keyword che permette di aggiungere dei mixin come SingleTickerProviderStateMixin
// dietro alle quinte la classe SingleTickerProviderStateMixin fornisce varie funzioni necessarie per le animazioni
// utilizziamo single perche utilizziamo un singolo animationController
class _GenerationsScreenState extends State<GenerationsScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _animationController = AnimationController(
      //questo parametro e' responsabile dell'esecuzione dell'animazione per ogni fotogramma (in genere 60 volte al secondo per fornire un'animazione fluida)
      vsync: this, //impostiamo come valore this ovvero questo widget
      duration: const Duration(
          microseconds: 300), // impostiamo la durata dell'animazione
      lowerBound: 0, // rappresentano i confini dell'animazione
      upperBound: 1,
    );

    _animationController.forward(); //avvia l'animazione
    //_animationController.repeat(); // ripete l'animazione una volta terminata
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _animationController.dispose();
    super.dispose();
  }

  void selectGeneration(BuildContext context, Generation generation) {
    final filteredPokemon = widget.availablePokemon
        .where((pokemon) =>
            pokemon.generation.toString().contains(generation.id.toString()))
        .toList();
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (ctx) => Pokemons(
          title: generation.title,
          pokemon: filteredPokemon,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // AnimatedBuilder dietro alle quinte dira' a animatedController quando chiamare la funzione passata (es ad ogni click ecc..)
    return AnimatedBuilder(
      animation: _animationController,
      child: GridView(
        padding: const EdgeInsets.all(16),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 2,
            crossAxisSpacing: 20,
            mainAxisSpacing: 20),
        children: [
          for (final item in generationsData)
            GenerationItem(
              generation: item,
              onSelectGeneration: () {
                selectGeneration(context, item);
              },
            )
        ],
      ),

      // all'interno di questa funzione e' possibile aggiornare il padding o il marging di un widget o qualsiasi altro valore
      builder: (context, child) =>
          // SlideTransition permette di far scorrere il widget da una direzione all'altra
          // drive e' un metodo usato per costruire un animazione
          // Tween si occupa di animare o descrivere la transizione tra due valori
          SlideTransition(
        position: _animationController.drive(
          Tween(
            // all'inizio dell'animazione il widget avra' uno scostamento del 30% sull'asse delle y
            begin: const Offset(0, 0.3),
            // alla fine dell'animazione il widget torna alla posizione avrebbe normarlmente
            end: const Offset(0, 0),
          ),
        ),
        // impostiamo GridView come child, significa che verra' visualizzato all'interno dell'animazione ma non verra' ricostruito 60 volte al secondo
        child: child,
      ),

      //////// STESSA ANIMAZIONE MA CON ANIMATE ////////
      //animate accetta un aromento usato per avere maggior controllo sul modo in cui l'animazione viene prodotta
      // builder: (context, child) => SlideTransition(
      //   position: Tween(
      //     // all'inizio dell'animazione il widget avra' uno scostamento del 30% sull'asse delle y
      //     begin: const Offset(0, 0.3),
      //     // alla fine dell'animazione il widget torna alla posizione avrebbe normarlmente
      //     end: const Offset(0, 0),
      //     //queste curve controllano il modo in cui la transazione viene distribuita
      //   ).animate(CurvedAnimation(
      //       parent: _animationController, curve: Curves.easeInOut)),
      //   child: child,
      // ),
      //////// FINE ANIMAZIONE CON ANIMATE ////////

      //////// STESSA AMINAMZIONE CON PADDING ////////
      // padding e' il widget effettivamente animato 60 volte al secondo.
      // il valore e' impostato dinamicamente in base al valore che lower e upper assumono
      // il padding parte dal valore 100 e alla fine dell'animazione sara' 0
      // builder: (context, child) => Padding(
      //   padding: EdgeInsets.only(top: 100 - _animationController.value * 100),
      //   child: child,
      // ),
      //////// FINE AMINAMZIONE CON PADDING ////////
      ///
      ///
    );
  }
}
