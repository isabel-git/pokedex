import 'package:flutter/material.dart';

enum Types {
  Normale,
  Fuoco,
  Acqua,
  Erba,
  Elettro,
  Ghiaccio,
  Lotta,
  Veleno,
  Terra,
  Volante,
  Psico,
  Coleottero,
  Roccia,
  Spettro,
  Drago,
  Buio,
  Acciaio,
  Folletto
}

extension TypesExtension on Types {
  Color get color {
    switch (this) {
      case Types.Normale:
        return Colors.grey;
      case Types.Fuoco:
        return Colors.red;
      case Types.Acqua:
        return Colors.blue;
      case Types.Erba:
        return Colors.green;
      case Types.Elettro:
        return Colors.yellow;
      case Types.Ghiaccio:
        return Colors.lightBlueAccent;
      case Types.Lotta:
        return Colors.brown;
      case Types.Veleno:
        return Colors.purple;
      case Types.Terra:
        return Colors.brown[300]!;
      case Types.Volante:
        return Colors.blue[100]!;
      case Types.Psico:
        return Colors.pink;
      case Types.Coleottero:
        return Colors.lightGreen;
      case Types.Roccia:
        return Colors.brown[600]!;
      case Types.Spettro:
        return Colors.indigo;
      case Types.Drago:
        return Colors.indigo[900]!;
      case Types.Buio:
        return Colors.black;
      case Types.Acciaio:
        return Colors.blueGrey;
      case Types.Folletto:
        return Colors.pinkAccent;
      default:
        return Colors.grey;
    }
  }
}

class Pokemon {
  const Pokemon({
    required this.id,
    required this.generation,
    required this.type,
    required this.name,
    required this.imageUrl,
    required this.description,
    required this.category,
    required this.isLegendary,
    required this.isMythical,
    required this.isStarter,
    required this.isRegionalVariant,
  });

  final String id;
  final int generation;
  final List<Types> type;
  final String name;
  final String imageUrl;
  final String description;
  final String category;
  final bool isLegendary; // Indica se il Pokémon è leggendario
  final bool isMythical; // Indica se il Pokémon è mitico
  final bool isStarter; // Indica se il Pokémon è un Pokémon iniziale
  final bool isRegionalVariant; // Indica se il Pokémon è una variante regionale
}
