import 'package:flutter/material.dart';

class Generation {
  Generation({
    required this.id,
    required this.title,
    required this.color,
  });
  final int id;
  final String title;
  final Color color;
}
