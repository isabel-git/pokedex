import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pokedex/models/pokemon.dart';

//it's optimized for dynamic data

class FavoritesPokemonNotifier extends StateNotifier<List<Pokemon>> {
  //dato che la classe e' generics dobbiamo indicare quali diti di dati dovra' gestire il notificatore
  // di seguito aggiungo all'interno di super il valore iniziale (un elenco vuoto da popolare sucessivamente)
  FavoritesPokemonNotifier()
      : super(
            []); // non potro' mai accedere direttamente a questa lista, dovro' sempre riassegnarlo con uno nuovo

  // di seguito aggiungo il metodo per aggiungere o rimuovere un pokemon dai preferiti
  bool toggleFavoritePokemon(Pokemon p) {
    final pokemonIsFavorite = state.contains(p);

    if (pokemonIsFavorite) {
      state = state.where((pokemon) => pokemon.id != p.id).toList();
      return false;
    } else {
      state = [...state, p];
      return true;
    }
  }
}

final favoritesProvider =
    StateNotifierProvider<FavoritesPokemonNotifier, List<Pokemon>>((ref) {
  return FavoritesPokemonNotifier(); // restituisce un istanza della classe notifier
});
