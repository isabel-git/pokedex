import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pokedex/data/generations_data.dart';

//it's optimized for static data
final pokemonsProvider = Provider((ref) {
  return pokemonsData;
});
