import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pokedex/providers/pokemons_provider.dart';

enum Filter {
  baseStats,
  legendary,
  mytical,
  regionalVariant,
}

//il notificatore e' una classe generica che dovra' gestire una mappa di filter
class FiltersNotifier extends StateNotifier<Map<Filter, bool>> {
  FiltersNotifier()
      //stato iniziale del  filtro impostato su false
      : super({
          Filter.baseStats: false,
          Filter.legendary: false,
          Filter.mytical: false,
          Filter.regionalVariant: false,
        });

  void setFilters(Map<Filter, bool> allFilters) {
    state = allFilters;
  }

  //definiao un metodo che ci permette di manipolare lo stato in maniera immutabile
  void setFilter(Filter f, bool isActive) {
    // state[f] = isActive; NON E' CONSENTITO MUTARE LO STATO

    // possiamo copiare la mappa esistente con l'operatore spread ...
    state = {
      ...state, // lo spread copia le mappe esistenti e sovrascrive la coppia di chiave-valore con il nuovo filtro
      f: isActive
    };
  }
}

final filtersProvider =
    StateNotifierProvider<FiltersNotifier, Map<Filter, bool>>((ref) {
  return FiltersNotifier();
});

final filteredPokemonsProvider = Provider((ref) {
  final pokemons = ref.watch(pokemonsProvider);
  final activeFilteres = ref.watch(filtersProvider);
  return pokemons.where((pokemon) {
    if (activeFilteres[Filter.baseStats]! && !pokemon.isStarter) {
      return false;
    }
    if (activeFilteres[Filter.legendary]! && !pokemon.isLegendary) {
      return false;
    }
    if (activeFilteres[Filter.mytical]! && !pokemon.isMythical) {
      return false;
    }
    if (activeFilteres[Filter.regionalVariant]! && !pokemon.isRegionalVariant) {
      return false;
    }

    return true;
  }).toList();
});
