import 'package:flutter/material.dart';
import 'package:pokedex/models/generation.dart';
import 'package:pokedex/models/pokemon.dart';

final generationsData = [
  Generation(
    id: 0,
    title: 'gen. 1',
    color: Colors.red,
  ),
  Generation(
    id: 1,
    title: 'gen. 2',
    color: Colors.orange,
  ),
  Generation(
    id: 2,
    title: 'gen. 3',
    color: Colors.green,
  ),
  Generation(
    id: 3,
    title: 'gen. 4',
    color: Colors.amber,
  ),
  Generation(
    id: 4,
    title: 'gen. 5',
    color: Colors.blue,
  ),
  Generation(
    id: 5,
    title: 'gen. 6',
    color: Colors.purple,
  ),
  Generation(
    id: 6,
    title: 'gen. 7',
    color: Colors.cyan,
  ),
  Generation(
    id: 7,
    title: 'gen. 8',
    color: Colors.indigo,
  ),
];

final pokemonsData = [
  // PRIMA GENERAZIONE
  const Pokemon(
    id: '#0001',
    generation: 0,
    type: [Types.Erba, Types.Veleno],
    name: 'Bulbasaur',
    imageUrl:
        'https://media.pokemoncentral.it/wiki/thumb/9/9b/Artwork0001_RFVF.png/600px-Artwork0001_RFVF.png',
    description:
        'Bulbasaur is a small, mainly turquoise amphibian Pokémon with red eyes and a green bulb on its back. It is based on a frog/toad, with the bulb resembling a plant bulb that grows into a flower as it evolves.',
    category: 'Pokemon Seme',
    isLegendary: false,
    isMythical: false,
    isRegionalVariant: false,
    isStarter: true,
  ),
  const Pokemon(
    id: '#0002',
    generation: 0,
    type: [Types.Erba, Types.Veleno],
    name: 'Ivysaur',
    imageUrl:
        'https://media.pokemoncentral.it/wiki/thumb/4/48/Artwork0002_RFVF.png/600px-Artwork0002_RFVF.png',
    description: 'description',
    category: 'Pokemon Seme',
    isLegendary: false,
    isMythical: false,
    isRegionalVariant: false,
    isStarter: false,
  ),

  //SECONDA GENERAZIONE
  const Pokemon(
    id: '#0197',
    generation: 1,
    type: [Types.Buio],
    name: 'Umbreon',
    imageUrl:
        'https://media.pokemoncentral.it/wiki/thumb/c/c7/Artwork0197_RFVF.png/600px-Artwork0197_RFVF.png',
    description: 'description',
    category: 'Pokemon Lucelunare',
    isLegendary: false,
    isMythical: false,
    isRegionalVariant: false,
    isStarter: false,
  ),

  //TERZA GENERAZIONE
  const Pokemon(
    id: '#0372',
    generation: 2,
    type: [Types.Drago],
    name: 'Shelgon',
    imageUrl: 'https://media.pokemoncentral.it/wiki/8/86/Artwork0372_RZ.png',
    description: 'description',
    category: 'Pokemon Resistenza',
    isLegendary: false,
    isMythical: false,
    isRegionalVariant: false,
    isStarter: false,
  ),
  //QUARTA GENERAZIONE
  const Pokemon(
    id: '#0493',
    generation: 3,
    type: [Types.Normale],
    name: 'Arceus',
    imageUrl:
        'https://media.pokemoncentral.it/wiki/thumb/f/f0/Artwork0493_Pt.png/600px-Artwork0493_Pt.png',
    description: 'description',
    category: 'Pokemon Misterioso',
    isLegendary: false,
    isMythical: false,
    isRegionalVariant: false,
    isStarter: false,
  ),
  //QUINTA GENERAZIONE
  const Pokemon(
    id: '#0649',
    generation: 4,
    type: [Types.Coleottero, Types.Acciaio],
    name: 'Genesect',
    imageUrl:
        'https://media.pokemoncentral.it/wiki/thumb/d/dd/Artwork0649_N2B2.png/600px-Artwork0649_N2B2.png',
    description: 'description',
    category: 'Pokemon Paleozoico',
    isLegendary: false,
    isMythical: true,
    isRegionalVariant: false,
    isStarter: false,
  ),

  //SESTA GENERAZIONE
  const Pokemon(
    id: '#0702',
    generation: 5,
    type: [Types.Elettro, Types.Folletto],
    name: 'Dedenne',
    imageUrl:
        'https://media.pokemoncentral.it/wiki/thumb/b/b1/Artwork0702_XY.png/600px-Artwork0702_XY.png',
    description: 'description',
    category: 'Pokemon Paleozoico',
    isLegendary: false,
    isMythical: true,
    isRegionalVariant: false,
    isStarter: false,
  ),
];
