import 'package:flutter/material.dart';
import 'package:pokedex/widget/main_drawer_item.dart';

class MainDrawer extends StatelessWidget {
  const MainDrawer({super.key, required this.onSelectScreen});

  final void Function(String identifier) onSelectScreen;

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          DrawerHeader(
            // padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Colors.amber,
                  Colors.amber.withOpacity(0.3),
                ],
              ),
            ),
            child: Image.asset(
              // width: 100,
              'assets/images/pokemon_logo.svg',
              // fit: BoxFit.cover,
            ),
          ),
          MainDrawerItem(
            icon: Icons.catching_pokemon,
            lable: 'Pokedex',
            onSelectScreen: onSelectScreen,
          ),
          MainDrawerItem(
            icon: Icons.settings,
            lable: 'Impostazioni',
            onSelectScreen: onSelectScreen,
          ),
        ],
      ),
    );
  }
}
