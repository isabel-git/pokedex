import 'package:flutter/material.dart';
import 'package:pokedex/models/pokemon.dart';

class PokemonItemTrait extends StatelessWidget {
  const PokemonItemTrait({super.key, required this.types});

  // final List<IconData> icon;
  final List<Types> types;
  @override
  Widget build(BuildContext context) {
    final content = types
        .map((type) => Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              color: type.color,
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                child: Text(
                  type.name,
                  style: const TextStyle(color: Colors.white),
                ),
              ),
            ))
        .toList();

    return Row(
      children: content,
    );
  }
}
