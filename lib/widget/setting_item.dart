import 'package:flutter/material.dart';

class SettingItem extends StatelessWidget {
  const SettingItem(
      {super.key,
      required this.title,
      required this.subtitle,
      required this.value,
      required this.handleChange});

  final String title;
  final String subtitle;
  final bool value;
  final void Function(bool) handleChange;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return SwitchListTile(
      value: value,
      onChanged: (newValue) {
        handleChange(newValue);
      },
      title: Text(
        title,
        style: theme.textTheme.titleLarge!.copyWith(
          color: theme.colorScheme.onSurface,
        ),
      ),
      subtitle: Text(
        subtitle,
        style: theme.textTheme.titleSmall!.copyWith(
          color: theme.colorScheme.onSurface,
        ),
      ),
      activeColor: Colors.amber,
      contentPadding: const EdgeInsets.only(left: 34, right: 22),
    );
  }
}
