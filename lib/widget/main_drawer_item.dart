import 'package:flutter/material.dart';

class MainDrawerItem extends StatelessWidget {
  const MainDrawerItem(
      {super.key,
      required this.icon,
      required this.lable,
      required this.onSelectScreen});

  final void Function(String identifier) onSelectScreen;
  final IconData icon;
  final String lable;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return ListTile(
      //set a widget that will be presented at the begining of this Text item
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        lable,
        style: theme.textTheme.titleSmall!.copyWith(
          color: Theme.of(context).colorScheme.onSurface,
          fontSize: 20,
        ),
      ),
      onTap: () {
        onSelectScreen(lable);
      },
    );
  }
}
