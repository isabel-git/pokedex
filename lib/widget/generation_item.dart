import 'package:flutter/material.dart';
import 'package:pokedex/models/generation.dart';

class GenerationItem extends StatelessWidget {
  const GenerationItem(
      {super.key, required this.generation, required this.onSelectGeneration});

  final Generation generation;
  final void Function() onSelectGeneration;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final String capitalize = generation.title[0].toUpperCase() +
        generation.title.substring(1).toLowerCase();
    return InkWell(
      onTap: onSelectGeneration,
      splashColor: theme.primaryColor,
      child: Container(
        // padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          gradient: LinearGradient(
            colors: [generation.color, generation.color.withOpacity(0.55)],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
        ),
        child: Stack(
          children: [
            Positioned(
              bottom: -40,
              right: -10,
              child: Opacity(
                opacity:
                    0.1, // Regola l'opacità per rendere l'immagine semi-trasparente
                child: Image.asset(
                  height: 150,
                  'assets/images/pokeball.png',
                  // fit: BoxFit.cover,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(18.0),
              child: Text(
                capitalize,
                style: theme.textTheme.titleLarge!.copyWith(
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
