import 'package:flutter/material.dart';
import 'package:pokedex/models/pokemon.dart';
import 'package:pokedex/widget/pokemon_item_trait.dart';
import 'package:transparent_image/transparent_image.dart';

class PokemonItem extends StatelessWidget {
  const PokemonItem(
      {super.key, required this.pokemon, required this.onSelectPokemon});
  final Pokemon pokemon;
  final void Function(Pokemon pokemon) onSelectPokemon;
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.all(8),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      clipBehavior: Clip
          .hardEdge, //rimuove il contenuto dei widget figli che esca dai confini
      elevation: 2,
      child: InkWell(
        onTap: () {
          onSelectPokemon(pokemon);
        },
        // permette di sovrapporre dei widget
        child: Stack(
          children: [
            // Hero permette di animare widget diversi
            Hero(
              tag: pokemon.id,
              // FadeInImage visualizza un'immagine sfumata
              child: FadeInImage(
                // MemoryImage forniamo in memoria un'immagine futura da visualizzare
                placeholder: MemoryImage(
                  kTransparentImage, // IMMAGINE FINALE DA CARICARE: kTransparentImage è un'immagine fittizia completamente strasparente fornita dal pacchetto transparent_image
                ),

                image: NetworkImage(
                  pokemon
                      .imageUrl, // PIMA IMMAGINE CARICATA: è un'immagine da rete
                ),
                // height: 200,
                width: double.infinity,
                fit: BoxFit
                    .cover, //questo parametro assicura che le immagini non verranno mai distorte
              ),
            ),
            Positioned(
              top: 0, //viene posizionato sul bordo inferiore del'immagine
              right: 0,
              child: PokemonItemTrait(types: pokemon.type),
            ),
            // definisce il modo il cui il contenitore può essere posizionato sopra l'immagine
            Positioned(
              bottom: 0, //viene posizionato sul bordo inferiore del'immagine
              left: 0, // valore del marige sinistro
              right: 0, // margine destro

              child: Container(
                color: Colors.black54,
                child: Column(
                  children: [
                    Text(
                      pokemon.name,
                      maxLines:
                          2, //limito la lunghezza del testo a non più di 2 righe
                      overflow: TextOverflow
                          .ellipsis, //controlla il comportaento del testo se supera le 2 righe
                      textAlign: TextAlign.center,
                      softWrap: true,
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
